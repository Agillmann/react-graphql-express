# Stack

## Back-end

+ module npm :

  + express
  + express-graphql
  + graphql
  + lodash
  + mongoose

## Env variable :

+ Database

  + DB
  + USER_DB
  + PASSWORD_DB

---

## Front-end

+ module npm :
  + creat-react-app
  + apollo-boost
  + react-apollo
