import React, { Component } from 'react';
import {graphql} from 'react-apollo';

import {getAdminsQuery} from '../queries/queries';

class AdminList extends Component {
  
  //affiche books
  displayAdmin(){
    var data = this.props.data
    console.log(data);
    if(data.loading){
      return (<div>Loading...</div>);
    }
    else {
      return data.admin.map(admin => {
        return (
          <li key={admin.id}>{admin.login}</li>
        );
      })
    }
  }
  render() {
    // console.log(this.props);
    return (
      <div>
        <h2>Admins</h2>
        <ul id="admin-list">
          {this.displayAdmin()}
        </ul>
      </div>
    );
  }
}

export default graphql(getAdminsQuery)(AdminList);
