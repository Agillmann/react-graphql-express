import React, { Component } from 'react';
import {graphql} from 'react-apollo';

import {getAdminsQuery} from '../queries/queries';
import AdminList from './AdminList';

class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      login: "",
      password: "",
      id: "",
      auth: false
    };
  }

  submitForm(e){
    e.preventDefault();
    var data = this.props.data;
    if(data.loading){
      return(
        <option>Loading..</option>
      );
    }
    else{
      console.log(data, "coucou");
      let i = 0;
      for(i = 0;i < data.admin.length; i++){
        if(this.state.login === data.admin[i].login && this.state.password === data.admin[i].password){
          console.log('connect');
          this.setState({
            login: data.admin[i].login,
            auth: true,
            id: data.admin[i].id
          });
          break;
        }
        else{
          console.log('echec');
        }
      }
    }
  };

  logout(e){
    e.preventDefault();
    this.setState({
      login: "",
      password: "",
      auth: false,
      id: ""
    });

  }
  render() {
    // console.log(this.props);
    if(this.state.auth){
      return(
        <div>
          <p>Connecter en tant que {this.state.login}</p>
          <form onSubmit={this.logout.bind(this)}>
            <AdminList/>
            <button>logout</button>
          </form>
        </div>
      );

    }else{
      return (
        <form id="login" onSubmit={this.submitForm.bind(this)}>

          <div className="field">
            <label>Login :</label>
            <input type="text" onChange={(e)=> this.setState({login: e.target.value})}/>
          </div>

          <div className="field">
            <label>Password :</label>
            <input type="password" onChange={(e)=> this.setState({password: e.target.value})}/>
          </div>

          <button>Connect</button>
        </form>
      );
    }
  }
}

export default graphql(getAdminsQuery)(Login);
