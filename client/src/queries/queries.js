import { gql } from 'apollo-boost';

const getAuthorsQuery = gql`
  {
    authors{
      name
      id
    }
  }
`

const getBooksQuery = gql`
  {
    books{
      name
      id
    }
  }
`

const getAdminsQuery = gql`
  {
    admin{
      login
      password
      id
    }
  }
`
export {getAuthorsQuery,getBooksQuery,getAdminsQuery};
