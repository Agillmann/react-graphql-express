import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// components
import BookList from './components/BookList';
import AuthorList from './components/AuthorList';
import AdminList from './components/AdminList';
import AddBook from './components/AddBook';
import Login from './components/Login';

//apollo client setup
const client = new ApolloClient({
  uri:'http://localhost:4000/graphql'
})


class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      auth: false
    };
  }
  render() {
    if(this.state.auth){
      return (
        <ApolloProvider client={client}>
          <div id="main">
            <h1>Crud mongodb</h1>
            <BookList/>
            <AuthorList/>
            <AddBook/>
            <Login/>
            <AdminList/>
          </div>
        </ApolloProvider>
      );
    }
    else{
      return (
        <ApolloProvider client={client}>
          <div id="main">
            <h1>Crud mongodb</h1>
            <BookList/>
            <AuthorList/>
            <AddBook/>
            <Login/>
          </div>
        </ApolloProvider>
      );
    }
  }
}

export default App;
