const express = require('express');
const graphqlHTTP = require('express-graphql');
const mongoose = require('mongoose');
const cors = require('cors');
const clui = require('clui');

//GraphQL schema
const schema = require('./schema/schema');

//Express app
const app = express();

//allow cross-origin requests
app.use(cors());

//database enc
const DB = "";
const USER_DB = "";
const PASSWORD_DB = "";
const MONGO_URL = `mongodb://${USER_DB}:${PASSWORD_DB}@${DB}`;

// Connection database
mongoose.connect(MONGO_URL, {
  auth: {
    user: USER_DB,
    password: PASSWORD_DB
  },
  useNewUrlParser: true
})
mongoose.connect(`mongodb://${USER_DB}:${PASSWORD_DB}@${DB}`, { useNewUrlParser: true });
mongoose.connection.once('open', () =>{
  const Spinner = clui.Spinner;
  let countdown = new Spinner('Now listening requests port 4000', ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);
  countdown.start();
  console.log('Connected to database\n');
});

app.use('/graphql',graphqlHTTP({
  schema,
  graphiql: true
}));

app.listen(4000, () => {

  console.log('');
});
