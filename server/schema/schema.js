const graphql = require('graphql');
const _ = require('lodash');

const Book = require('../models/book');
const Author = require('../models/author');
const Admin = require('../models/admin');

const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLBoolean,
  GraphQLNonNull,
  GraphQLSchema
} = graphql;

// // dummy data
// const books = [
//   {name: 'Name of the Wind',genre:'Fantasy',id:'1',authorId:'1'},
//   {name: 'The Final Empire',genre:'Fantasy',id:'2',authorId:'2'},
//   {name: 'The Long Earth',genre:'Sci-Fi',id:'3',authorId:'3'},
//   {name: 'The Hero of Ages',genre:'Fantasy',id:'4',authorId:'2'},
//   {name: 'The Colour Magic',genre:'Fantasy',id:'5',authorId:'3'},
//   {name: 'The Big Light',genre:'Sci-Fi',id:'6',authorId:'3'},
// ];
//
// const authors = [
//   {name: 'Patrick R',age:50,id:'1'},
//   {name: 'Brandon S',age:42,id:'2'},
//   {name: 'Terry P',age:68,id:'3'},
// ];


const BookType = new GraphQLObjectType({
  name:'Book',
  fields: () => ({
    id: {type: GraphQLID},
    name: {type: GraphQLString},
    genre: {type: GraphQLString},
    author: {
      type: AuthorType,
      resolve(parent, args){
        // return _.find(authors,{id:parent.authorId});
        return Author.findById(parent.authorId);
      }
    },
  })
});

const AuthorType = new GraphQLObjectType({
  name:'Author',
  fields: () => ({
    id: {type: GraphQLID},
    name: {type: GraphQLString},
    age: {type: GraphQLInt},
    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args){
        // return _.filter(books,{authorId:parent.id})
        return Book.find({authorId: parent.id});
      }
    }
  })
});

const AdminType = new GraphQLObjectType({
  name:'Admin',
  fields: () => ({
    id: {type: GraphQLID},
    login: {type: GraphQLString},
    password: {type: GraphQLString},
    auth: {type: GraphQLBoolean}
  }),
  resolve(parent, args){
    // return _.filter(books,{authorId:parent.id})
    return Admin.find({});
  }
});

const RootQuery = new GraphQLObjectType({
  name:'RootQuery',
  fields: {
    book: {
      type: BookType,
      args:{id:{type: GraphQLID}},
      resolve(parent, args){
        //code to get data from db / other source
        // return _.find(books,{id:args.id});
        return Book.findById(args.id);
      }
    },
    author: {
      type: AuthorType,
      args:{id:{type: GraphQLID}},
      resolve(parent, args){
        // return _.find(authors,{id:args.id});
        return Author.findById(args.id);
      }
    },
    books: {
      type: new GraphQLList(BookType),
      resolve(parent, args){
        // return books
        return Book.find({});
      }
    },
    authors: {
      type: new GraphQLList(AuthorType),
      resolve(parent, args){
        // return authors
        return Author.find({});
      }
    },
    admin: {
      type: new GraphQLList(AdminType),
      resolve(parent, args){
        // return authors
        return Admin.find({});
      }
    }
  }
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    addAuthor: {
      type: AuthorType,
      args: {
        name:{type:new GraphQLNonNull(GraphQLString)},
        age:{type:new GraphQLNonNull(GraphQLInt)}
      },
      resolve(parent, args){
        let author = new Author({
          name: args.name,
          age: args.age
        });
        return author.save();
      }
    },
    addBook: {
      type: BookType,
      args: {
        name:{type:new GraphQLNonNull(GraphQLString)},
        genre:{type:new GraphQLNonNull(GraphQLString)},
        authorId:{type:new GraphQLNonNull(GraphQLID)}
      },
      resolve(parent, args){
        let book = new Book({
          name: args.name,
          genre: args.genre,
          authorId: args.authorId
        });
        return book.save();
      }
    },
    addAdmin: {
      type: AdminType,
      args: {
        login:{type:new GraphQLNonNull(GraphQLString)},
        password:{type:new GraphQLNonNull(GraphQLString)},
      },
      resolve(parent, args){
        let admin = new Admin({
          login: args.login,
          password: args.password,
          auth: false
        });
        return admin.save();
      }
    }
  }
})

module.exports = new GraphQLSchema({
  query:RootQuery,
  mutation: Mutation
});
