const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const adminSchema = new Schema({
  login: String,
  password: String,
  auth: Boolean,
});

module.exports = mongoose.model('Admin',adminSchema);
